package com.reltio.lca;

import com.fasterxml.reltio.jackson.databind.JsonNode;
import com.fasterxml.reltio.jackson.databind.ObjectMapper;
import com.google.common.io.Files;
import com.reltio.lifecycle.server.core.services.LifeCycleHook;
import com.reltio.lifecycle.test.LifecycleExecutor;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.Charset;

import static org.junit.Assert.assertEquals;

public class BeforeSaveActionSampleTest {
    private LifecycleExecutor executor = new LifecycleExecutor();
    private BeforeSaveActionSample handler = new BeforeSaveActionSample();

    @Test
    public void test() throws URISyntaxException, IOException {
        String input = Files.toString(new File(getClass().getResource("/input.json").toURI()), Charset.defaultCharset());

        String result = executor.executeAction(handler, LifeCycleHook.beforeSave, input);

        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode jsonNode = objectMapper.readValue(result, JsonNode.class);
        String name = jsonNode.get("object").get("attributes").get("Name").elements().next().get("value").asText();
        assertEquals("John Snow", name);
    }

    @Test
    public void testWithExistingFullName() throws URISyntaxException, IOException {
        String input = Files.toString(new File(getClass().getResource("/input_with_full_name.json").toURI()), Charset.defaultCharset());

        String result = executor.executeAction(handler, LifeCycleHook.beforeSave, input);

        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode jsonNode = objectMapper.readValue(result, JsonNode.class);
        String name = jsonNode.get("object").get("attributes").get("Name").elements().next().get("value").asText();
        assertEquals("Existing Name", name);
    }
}
